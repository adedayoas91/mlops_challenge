This project demonstrates continuos integration (CI) using Jenkins  

## Main Task
- Install Jenkins on an AWS server
- Dockerize the application
- Deploy it
- Create a Jenkins pipeline to do the following on merge to master:
    - Run the tests
    - Deploy (and redeploy) bonus: deploy with zero-downtime